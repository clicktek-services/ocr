<?php

namespace App\Helpers;

use Aws\Textract\TextractClient;
use Dflydev\DotAccessData\Data;
use Illuminate\Support\Facades\Config;
use DB;
use App\Helpers\AWSHelper1;
use mysql_xdevapi\Exception;
use function Couchbase\basicEncoderV1;

class AWSHelper
{
  private $textKeys;



  static function convert()
  {
    $template = AWSHelper1::ORCTemplate();
    $attribute = [];
    foreach ($template['key'] as $value) {
      $attribute[] = [
        "code" => $value['field_id'],
        "label" => $value['label'][0]['fulltext'],
        "type" => "string",
        "keyword" => [
          "primary" => $value['value'][0]['fulltext'],
          "secondary" => ['#'],
        ],
        "neighbor" => [
          '1nvoice', 'Involce'
        ],
        "proximity" => "1.1",
        "bounding_box" => [
          "width" => "0.046430289745331",
          "height" => "0.063324727118015",
          "left" => "0.096519589424133",
          "top" => "0.0078962240368128"
        ],
        "value" => [
          "text" => $value['value'][0]['fulltext'],
          "bounding_box" => [
            "Width" => "0.20822890102863",
            "Height" => "0.015255242586136",
            "Left" => "0.57872796058655",
            "Top" => "0.20535565912724"
          ]
        ]
      ];
    dd($attribute[0]);
    }



    $data = [
      'company_name' => $template['company_name'],
      'description' => $template['company_name']
    ];


  }

  static function validateTemplate($textract, $templates) {
    foreach ($templates as $templateKey => $template) {
      $score[$templateKey]['key_value'] = $templateKey;
      $score[$templateKey]['score'] =0;
      foreach ($template['document_key'] as $keyword) {
        $textKeys = self::searchTextOnTextract($textract, $keyword);
        if (!$textKeys) {
          $textKeys = self::searchLowConfidence($textract, $keyword);
        }
        if (!empty($textKeys)){
          $score[$templateKey]['score'] += 1;
        }
      }
    }
    $score = self::removeByScore($score);
    if (count($score) == 1){
      return $score[0]['key_value'];
    }
  }


  static function extractData($textract, $model){
   // dd($model);
    foreach ($model['attribute'] as $key => $item) {
      $information = [];
      $keyword = $item['keyword']['primary'];
      $textKeys = self::searchTextOnTextract($textract, $keyword);
      $information['primary'] = [
        'keyword' => $keyword,
        'result' => $textKeys
      ];
      if (!$textKeys) {
        $textKeys = self::searchLowConfidence($textract, $keyword);
        $information['LowConfidence'] = [
          'keyword' => $keyword,
          'result' => $textKeys
        ];
      }
      //dd($textKeys);
      $secondaryKeyword = $item['keyword']['secondary'];
      if (count($textKeys) == 1) {
        $model['attribute'][$key]['value'] = self::setModelValue($textract, $item, $textKeys, $information);
      } elseif ($secondaryKeyword != []) {
        $secondaryLowConfidenceKey = [];
        $secondaryLowConfidenceResult = [];
        $secondaryKeyWordKey = [];
        $secondaryKeywordResult = [];
        if (!empty($textKeys)) {
          $temporaryTextKeys = $textKeys;
          $removeDuplicate = 0;
        } else {
          $temporaryTextKeys = $textract;
          $removeDuplicate = 1;
        }
        //dd($temporaryTextKeys);
        foreach ($secondaryKeyword as $sw => $value) {
          $results = self::searchTextOnTextract($temporaryTextKeys, $value, $removeDuplicate);
          //$results = self::searchTextOnTextract($temporaryTextKeys, $value);
         //dd($results);
          if (!empty($results)) {
            $secondaryKeyWordKey[] = $value;
            $secondaryKeywordResult = $results;
          } else {
            $results = self::searchLowConfidence($temporaryTextKeys, $value);
            $secondaryLowConfidenceKey[] = $value;
            $secondaryLowConfidenceResult[] = $results;
          }
          foreach ($results as $rr => $result) {
            if (array_column($textKeys, 'keyValue')) {
              $arrayKey = array_search($result['keyValue'], array_column($textKeys, 'keyValue'));
              if (is_numeric($arrayKey)) {
                $textKeys[$arrayKey]['score'] += 1;
              } else {
                $result['score'] += 1;
                $textKeys[] = $result;
              }
            } else {
              $result['score'] += 1;
              $textKeys[] = $result;
            }
          }
        }
        $information['secondaryKeyword'] = [
          'keyword' => $secondaryKeyWordKey,
          'choices' => $secondaryKeywordResult,
          'result' => $textKeys,
        ];
        if ($secondaryLowConfidenceKey) {
          $information['secondaryKeywordLowConfidence'] = [
            'keyword' => $secondaryLowConfidenceKey,
            'choices' => $secondaryLowConfidenceResult,
            'result' => $textKeys
          ];
        }
        $textKeys = self::removeByScore($textKeys);
      }
      if (count($textKeys) == 1) {
        $model['attribute'][$key]['value'] = self::setModelValue($textract, $item, $textKeys, $information);
      } elseif (count($textKeys) > 1) {
        foreach ($item['keyword']['neighbor'] as $neighborKey => $value) {
          $neighbor = self::searchTextOnTextract($textract, $value);
          if (empty($neighbor)) {
            $neighbor = self::searchLowConfidence($textract, $value);
          }
          $textKeys = self::neighborKeyWord($textKeys, $neighbor);
          $information['neighborKeyWord'][] = [
            'keyword' => $value,
            'neighbor' => $neighbor,
            'result' => $textKeys
          ];
        }
        $textKeys = self::removeByScore($textKeys);
        if (count($textKeys)) {
          $model['attribute'][$key]['value'] = self::setModelValue($textract, $item, $textKeys, $information);
        } else {
          $model['attribute'][$key]['value'] = 'No Match Found';
        }
      }
    }
    $model['table'] = self::extractLineItem($model, $textract);
    return $model;
  }

  public static function extractLineItem($model, $textract = [])
  {
//dd($model);
    //$textract = AWSHelper1::ORCData();
    //$textract = AWSHelper1::ORCData1();
    //$textract = AWSHelper1::ORCData2();
    //$textract = self::textract();
    //print_r($textract);
    //dd($textract);
    $tables = array_keys(array_column($textract, 'BlockType'), 'TABLE');

    foreach ($tables as $table) {
      $tabs[] = $textract[$table];
    }
    $cells = array_keys(array_column($textract, 'BlockType'), 'CELL');
    foreach ($model['table'] as $tableKey => $table) {
      $keyword = $table['text'];
      $textKeys = self::searchTextOnTextract($textract, $keyword);
      if (!$textKeys) {
        $textKeys = self::searchLowConfidence($textract, $keyword);
      }
      foreach ($textKeys as $key => $textKey) {
        if (isset($textKey['Relationships'][0]['Ids'])) {
          $idCount = count($textKey['Relationships'][0]['Ids']);
          if ($idCount > 1) {
            $per = 0;
            $id = '';
            foreach ($textKey['Relationships'][0]['Ids'] as $wordId) {
              $word = $textract[array_search($wordId, array_column($textract, 'Id'))]['Text'];
              $similar = similar_text($keyword, $word, $perc);
              if ($per < $perc) {
                $per = $perc;
                $id = $wordId;
              }
            }
          } else {
            $id = $textKey['Relationships'][0]['Ids'][0];
          }
        }
        foreach ($cells as $cell) {
          if (isset($textract[$cell]['Relationships'][0]['Ids'])) {
            $ids = $textract[$cell]['Relationships'][0]['Ids'];
            $nn = array_search($id, $ids);
            if ($nn != []) {
              $model['table'][$tableKey]['column'][] = $textract[$cell];
              foreach ($tabs as $tab) {
                $nn1 = array_search($textract[$cell]['Id'], $tab['Relationships'][0]['Ids']);
                if ($nn1 != []) {
                  if (isset($model['texTractTable'][$tab['Id']])) {
                    $textExist = array_search($table['text'], array_column($model['texTractTable'][$tab['Id']], 'text'));
                  } else {
                    $textExist = null;
                  }
                  if (!is_numeric($textExist)) {
                    $model['texTractTable'][$tab['Id']][] = [
                      'text' => $table['text'],
                      'condition' => $table['condition'],
                      'textTract' => $textract[$cell],
                    ];
                  } else {
                    if ($model['texTractTable'][$tab['Id']][$textExist]['textTract']['RowIndex'] > $table['rowIndex']) {
                      $model['texTractTable'][$tab['Id']][] = [
                        'text' => $table['text'],
                        'condition' => $table['condition'],
                        'textTract' => $textract[$cell],
                      ];

                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    //dd($model);
    $count = 0;
    $newTable = [];
    $tableId = '';
    if (isset($model['texTractTable'])) {
      foreach ($model['texTractTable'] as $tableKey => $table) {
        if ($count < array_keys($table)) {
          $count = array_keys($table);
          $newTable = $table;
          $tableId = $tableKey;
        }
      }
      $columnIndex = [];
      $columnData = [];
      //dd($newTable);
      foreach ($newTable as $value) {
        $columnIndex[] = $value['textTract']['ColumnIndex'];
        $columnData[] = $value['textTract'];

      }
      $table = $textract[array_search($tableId, array_column($textract, 'Id'))];
      foreach ($table['Relationships'][0]['Ids'] as $value) {
        $column = $textract[array_search($value, array_column($textract, 'Id'))];
        //dd($column);
        if (isset($column['Relationships'][0]['Ids'])) {
          $exist = array_search($column['ColumnIndex'], array_column($columnData, 'ColumnIndex'));
          if (is_numeric($exist)) {
            if (intval($columnData[$exist]['RowIndex']) < intval($column['RowIndex'])) {
              $textValue = '';
              if ($newTable[$exist]['condition']['line'] == []) {
                foreach ($column['Relationships'][0]['Ids'] as $id) {
                  $textValue = $textValue . $textract[array_search($id, array_column($textract, 'Id'))]['Text'] . ' ';
                }
              } else {
                $textValue = self::lineSeparation($textract, $column['Id'], $newTable[$exist]['condition']['line']);
                //dd($textValue);
              }

              if ($textValue != '')
                $newTable[$exist]['lineItem'][] = $textValue;
            }
          }
        }
      }
    }

    //dd($newTable);
    return $newTable;
    dd('No Table Found');
  }

  static function lineSeparation($textract, $columnId, $getLine)
  {
    $lines = array_keys(array_column($textract, 'BlockType'), 'LINE');
    $textractLineIds = [];
    $textractLine = [];
    $lineCount = 0;
    $table1 = $textract[array_search($columnId, array_column($textract, 'Id'))];
    foreach ($table1['Relationships'][0]['Ids'] as $tableKey => $id) {
      if (!in_array($id, $textractLineIds)) {
        $table2 = $textract[array_search($id, array_column($textract, 'Id'))];
        foreach ($lines as $line) {
          if (in_array($table2['Id'], $textract[$line]['Relationships'][0]['Ids'])) {
            $textractLineIds = $textract[$line]['Relationships'][0]['Ids'];
            if (in_array($lineCount, $getLine)) {
              $textractLine[$lineCount] = $textract[$line]['Text'] . ' ';
            }
            $lineCount += 1;
          }
        }
      }
    }
    return $textractLine;
  }

  static function awsTextract($file_path)
  {
   // return AWSHelper1::ORCData();
    //$file_path = 'ecm/staging/invoice_2.png';
    //$file_path = 'ecm/staging/1619708693.pdf';//acd
    //$file_path = 'ecm/staging/invoice_sample_1019-1.jpg';//acd
    //$file_path = 'ecm/staging/1619678990.jpg';//your logo
   // $file_path = 'ecm/staging/1620047473.jpg';//acd jpg
    $client = new TextractClient([
      'region' => Config::get('aws.region'),
      'version' => Config::get('aws.version'),
      'credentials' => [
        'key' => Config::get('aws.credentials.key'),
        'secret' => Config::get('aws.credentials.secret')
      ]
    ]);
    $file_type = substr(strrchr($file_path, '.'), 1);
    if ($file_type == 'png' || $file_type == 'jpeg') {
      $options = [
        'Document' => [
          //'Bytes'=> $contents,
          'S3Object' => [
            'Bucket' => 'ecm-web-storage',
            'Name' => $file_path,
            //'Version'=> '<string>',
          ],
        ],
        'FeatureTypes' => ['TABLES'], // REQUIRED
      ];
      $result = $client->analyzeDocument($options);
    } else {
      $options = [
        'DocumentLocation' => [
          'S3Object' => [
            'Bucket' => 'ecm-web-storage',
            'Name' => $file_path,
          ],
        ],
        'FeatureTypes' => ['TABLES'], // REQUIRED
      ];
      $job = $client->startDocumentAnalysis($options)->toArray();
      if ($job['@metadata']['statusCode'] == 200) {
        sleep(5);
        $result = $client->getDocumentAnalysis(["JobId" => $job['JobId']])->toArray();
        while ($result['JobStatus'] == 'IN_PROGRESS') {
          sleep(5);
          $result = $client->getDocumentAnalysis(["JobId" => $job['JobId']])->toArray();
        }
      }
    }
    $data = $result['Blocks'];
    return $data;
  }

  static function removeByScore($textKeys)
  {
    if (!empty($textKeys)) {
      try {
        $maxScore = max(array_column($textKeys, 'score'));
      } catch (\ErrorException $error) {
        dd($textKeys);
      }
      $maxCount = array_keys(array_column($textKeys, 'score'), $maxScore);
      $result = [];
      if ($maxScore > 0 && !isset($maxCount[1])) {
        $result[] = $textKeys[array_search($maxScore, array_column($textKeys, 'score'))];
      } elseif ($maxScore > 0) {
        foreach ($maxCount as $value) {
          $result[] = $textKeys[$value];
        }
      } else {
        $result = $textKeys;
      }
    } else {
      $result = [];
    }
      return $result;

  }

  static function removeDuplicateWithScore($textKeys)
  {
    if (!isset($textKeys['Id']))
      $textKeys = call_user_func_array('array_merge', $textKeys);

    $hash = [];
    $arrayOut = [];
    foreach ($textKeys as $item) {
      $hash_key = $item['Id'];
      if (!array_key_exists($hash_key, $hash)) {
        // dd($arrayOut);
        $hash[$hash_key] = sizeof($arrayOut);
        array_push($arrayOut, $item);
      }
      $arrayOut[$hash[$hash_key]]['score'] += 1;
    }
    return $arrayOut;
  }

  static function searchTextOnTextract($textract, $keyword, $removeDuplicate = 1)
  {

    $input = preg_quote($keyword, '~');
    $textKeys = preg_grep('~' . $input . '~', array_column($textract, 'Text'));
    //dd($textKeys);
    if ($removeDuplicate) {
      $removeKeys = [];
      foreach ($textKeys as $key1 => $textKey) {
        $data = $textract[$key1 + 1];
        if (isset($data['Relationships'])) {
          foreach ($data['Relationships'] as $relation) {
            foreach ($relation['Ids'] as $id) {
              $removeKeys[] = (array_search($id, array_column($textract, 'Id')) - 1);
            }
          }
        }
      }
      $textKeys = array_diff_key($textKeys, array_flip($removeKeys));
    }

    if (!$removeDuplicate){
     // dd($textract);
    }
    $result = [];
    foreach ($textKeys as $textKey => $value) {

      if (!isset($textract[$textKey]['keyValue'])) {
        $textract[$textKey + 1]['keyValue'] = $textKey + 1;
        $textract[$textKey + 1]['score'] = 0;
        $result[] = $textract[$textKey + 1];

      } else {
        //$textract[$textKey]['score'] += 1;
        $result[] = $textract[$textKey];
      }
      if (!$removeDuplicate){
     //   print_r($result);
      }
    }
    if (!$removeDuplicate){
    //  dd($result);
    }
    //dd($result);
    $result = array_values($result);

    return $result;
  }

  static function searchText($array, $keyword)
  {
    return preg_grep('~' . preg_quote($keyword, '~') . '~', $array);
  }

  static function setModelValue($textract, $model, $textKeys, $information)
  {
    $keywords = implode(' ', [$model['keyword']['primary']]);
    $keywords = $keywords . ' ' . implode(' ', $model['keyword']['secondary']);
    $keywords = $keywords . ' ' . implode(' ', $model['keyword']['neighbor']);
    $keywords = explode(' ', $keywords);
//dd($model['attribute_value']['bounding_box']);
    $location = self::valueLocation($model['bounding_box'], $model['attribute_value']['bounding_box']);
    $information['valueLocation'] = $location;
    //  dd($location);
    $multiLine = 0;

      $words = $textKeys[0]['Text'];

    $value = '';
    $textractKey = $textKeys[0]['keyValue'] + 1;
    $keyBoundingBox = $textKeys[0]['Geometry']['BoundingBox'];
    A:
    $words = explode(' ', $words);
    foreach ($words as $word) {
      if (!in_array($word, $keywords)) {
        $percentage = 0;
        foreach ($keywords as $keyword) {
          similar_text($keyword, $word, $perc);
          if ($percentage < $perc) {
            $percentage = $perc;
          }
        }
        if ($perc < 80) {
          $value = $value . ' ' . $word;
        }
      }
    }

    if ($value == '' && isset($textract[$textractKey])) {
      $val = self::closeValueLocation($textract, $textractKey, $keyBoundingBox, $location);
      if (is_numeric($val)) {
        $textractKey = $val;
        $words = $textract[$textractKey]['Text'];
        goto A;
      } /*else {
        $textractKey += 1;
        goto A;
      }*/

    } /*elseif ($multiLine) {
      dd(321);
      C:
      $keyBoundingBox = $textract[$textractKey]['Geometry']['BoundingBox'];
      $con = $textractKey + 5;
      $i = $textractKey + 1;
      for ($i; $i <= $con; $i++) {
        if (isset($textract[$i])) {
          $bounding = $textract[$i]['Geometry']['BoundingBox'];
          if (($keyBoundingBox['Left'] - 0.005) <= $bounding['Left'] &&
            ($keyBoundingBox['Left'] + 0.005) >= $bounding['Left']) {
            $value = $value . " \n" . $textract[$i]['Text'];
            $textractKey = $i;
            goto C;
          }
        }
      }

    }*/
    $data['value'] = $value;
    //$data['TextractValue'] = $textract[$textractKey];
    $data['textract'] = $textKeys;
    $data['information'] = $information;

    return $data;
  }

  static function closeValueLocation($textract, $textractKey, $labelBoundingBox, $location)
  {
    $i = $textractKey;
    $ii = $i + 10;
    $candi = [];
    for ($i; $i < $ii; $i++) {
      if (isset($textract[$i])) {
        $valueBoundingBox = $textract[$i]['Geometry']['BoundingBox'];
        $newValueLocation = self::valueLocation($labelBoundingBox, $valueBoundingBox);
        if ($location === $newValueLocation) {
          $candi[$i] = $textract[$i];
        }
      } else {
        break;
      }
    }
    $geoLoc = 10;
    $closes = '';
    foreach ($candi as $key => $value) {
      if ($location['horizontal'] == 'left') {
        $left = $labelBoundingBox['Left'] - $value['Geometry']['BoundingBox']['Left'];
      } else {
        $left = $value['Geometry']['BoundingBox']['Left'] - $labelBoundingBox['Left'];
      }
      if ($location['vertical'] == 'bottom') {
        $top = $value['Geometry']['BoundingBox']['Top'] - $labelBoundingBox['Top'];
      } elseif ($location['vertical'] == 'top') {
        $top = $labelBoundingBox['Top'] - $value['Geometry']['BoundingBox']['Top'];
      } else {
        $top = 0;
      }

      if ($geoLoc > ($left + $top)) {
        $geoLoc = $left + $top;
        $closes = $key;
      }
    }
    return $closes;
  }

  static function valueLocation($label_bounding_box, $value_bounding_box)
  {
    if (floatval($label_bounding_box['Left']) > floatval($value_bounding_box['Left'])) {
      $location['horizontal'] = 'left';
    } else {
      $location['horizontal'] = 'right';
    }
    if (($label_bounding_box['Top'] < $value_bounding_box['Top'] + ($value_bounding_box['Height'] / 2)) && ($label_bounding_box['Top'] + $label_bounding_box['Height'] + ($label_bounding_box['Height'] / 2)) > $value_bounding_box['Top']) {
      $location['vertical'] = 'line';
    } elseif (($label_bounding_box['Top'] + $label_bounding_box['Height']) < $value_bounding_box['Top']) {
      $location['vertical'] = 'bottom';
    } else {
      $location['vertical'] = 'top';
    }
    return $location;
  }

  private function left($label, $value)
  {
    return $label > $value;
  }

  private function right($label, $value)
  {
    return $label < $value;
  }

  static function removeElementWithValue($array, $key, $value)
  {
    foreach ($array as $subKey => $subArray) {
      if ($subArray[$key] == $value) {
        unset($array[$subKey]);
      }
    }
    return array_values($array);
  }


  static function removeDuplicate($textract, $textKeys)
  {
    $removeKeys = [];
    foreach ($textKeys as $key1 => $textKey) {
      $data = $textract[$key1 + 1];
      if (isset($data['Relationships'])) {
        foreach ($data['Relationships'] as $relation) {
          foreach ($relation['Ids'] as $id) {
            $removeKeys[] = (array_search($id, array_column($textract, 'Id')) - 1);
          }
        }
      }
    }
    return array_diff_key($textKeys, array_flip($removeKeys));
  }

  static function searchLowConfidence($textract, $keyword)
  {
    //dd($keyword);
    $threshold = 90;
    $thresholdSimilarity = 60;
    $lowConfidences = array_filter($textract, function ($n) use ($threshold) {
      if (isset($n['Confidence']) && $n['BlockType'] == 'LINE') {
        return (floatval($n['Confidence']) <= $threshold);
      }
    });
    $sim = [];
    $percentage = [];
    $confidenceKeys = [];
    foreach ($lowConfidences as $lowConfidenceKey => $lowConfidence) {
      $lowConfidenceString = explode(' ', $lowConfidence['Text']);
      foreach ($lowConfidenceString as $explode => $word) {
        $sim[$lowConfidenceKey][$explode]['similarity'] = similar_text($keyword, $word, $perc);
        $sim[$lowConfidenceKey][$explode]['percentage'] = $perc;
        $percentage[$explode] = $perc;
        $information['Low_Confidence'][$lowConfidence['Text']][$word]['similarity'] = $sim[$lowConfidenceKey][$explode]['similarity'];
        $information['Low_Confidence'][$lowConfidence['Text']][$word]['percentage'] = $perc;
      }
      $confidenceKeys[$lowConfidenceKey] = max($percentage);
    }
    $result = [];
    foreach ($confidenceKeys as $confidenceKey => $value) {
      if ($thresholdSimilarity <= $value) {
        $result[$confidenceKey] = $textract[$confidenceKey];
        $result[$confidenceKey]['similarity'] = $value;
        if (!isset($result[$confidenceKey]['keyValue'])) {
          $result[$confidenceKey]['keyValue'] = $confidenceKey;
          $result[$confidenceKey]['score'] = 0;
        }
      }
    }

    $result = array_values($result);
    return $result;
  }

  static function neighborKeyWord($textKeys, $neighborKeys)
  {
    foreach ($textKeys as $textKey => $value) {
      $boundingBox = $value['Geometry']['BoundingBox'];
      $proximity = 0.1;
      $top = ($boundingBox['Top'] - $proximity);
      //$top = ($boundingBox['Top']);
      $height = ($boundingBox['Top'] + $boundingBox['Height'] + $proximity);
      $left = ($boundingBox['Left'] - $proximity);
      $width = (($boundingBox['Width'] + $boundingBox['Left']) + $proximity);
      foreach ($neighborKeys as $neighborKey => $neighbor) {
        $neighborWidth = $neighbor['Geometry']['BoundingBox']['Width'];
        $neighborHeight = $neighbor['Geometry']['BoundingBox']['Height'];
        $neighborLeft = $neighbor['Geometry']['BoundingBox']['Left'];
        $neighborTop = $neighbor['Geometry']['BoundingBox']['Top'];
        if (($top <= $neighborTop && $height >= $neighborTop) ||
          ($top <= $neighborTop + $neighborHeight && $height >= $neighborTop + $neighborHeight)) {
          if (($left <= $neighborLeft && $width >= $neighborLeft) ||
            ($left <= $neighborLeft + $neighborWidth && $width >= $neighborLeft + $neighborWidth)) {
            if ($textKeys[$textKey]['keyValue'] != $neighbor['keyValue']) {
              $textKeys[$textKey]['score'] += 1;
            }
          }
        }
      }
    }
    return $textKeys;
  }






}
