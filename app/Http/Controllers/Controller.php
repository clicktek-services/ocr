<?php

namespace App\Http\Controllers;

use App\Helpers\AWSHelper;
use App\Helpers\AWSHelper2;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Psr7\str;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


  public function test()
  {
    //$textract = AWSHelper::awsTextract('ecm/staging/Hloom_2.png');
    //$templates = DB::table('textracts')->insert([$textract]);
    //dd(1);
    //$templates = DB::table('template')->first();
    //dd($templates);
    $textract = DB::table('textracts')->first();

    $templates = [

      "_id" => "618a2f6dbc60176e34645a0c",
      "company_name" => "acd",
      "key_table" => [],
      "key" => [
        [
          "field_id" => "document_number",
          "label" => [
            "fulltext" => "Invoice",
            "word_ids" => [
              "aa6826af-eb52-4f4a-b293-602cf923f495"
            ]
          ],
          "value" => [
            "fulltext" => "120",
            "word_ids" => [
              "05659b57-955b-4098-93b3-d369a3abf7d3"
            ]
          ]
        ],

      ]
    ];

    //  dd($templates);

    $lines = array_keys(array_column($textract, 'BlockType'), 'LINE');
    $allValues = [];
    foreach ($templates['key'] as $attribute) {
      array_push($allValues, explode(' ', $attribute['value']['fulltext']));
    }
    $allValues = $this->multiArrayToSingle($allValues);

    foreach ($templates['key'] as $attribute) {

      $attributeValue = $textract[array_search($attribute['value']['word_ids'][0], array_column($textract, 'Id'))];
      $primaryKey = [];
      foreach ($lines as $line) {
        if (in_array($attribute['label']['word_ids'][0], $textract[$line]['Relationships'][0]['Ids'])) {
          $primaryKey = $textract[$line];
          break;
        }
      }
      $keywords = implode(' ', [$attribute['label']['fulltext']]);
      $keywords = $keywords . ' ' . implode([$attribute['value']['fulltext']]);
      $keywords = explode(' ', $keywords);
      $secondaryKeywords = explode(' ', $primaryKey['Text']);
      foreach ($keywords as $keyword) {
        if (($key = array_search($keyword, $secondaryKeywords)) !== false) {
          unset($secondaryKeywords[$key]);
        }
      }
      $proximity = 0.01;
      Proximity:
      $boundingBox = $primaryKey['Geometry']['BoundingBox'];
      $top = ($boundingBox['Top'] - $proximity);
      $height = ($boundingBox['Top'] + $boundingBox['Height'] + $proximity);
      $left = ($boundingBox['Left'] - $proximity);
      $width = (($boundingBox['Width'] + $boundingBox['Left']) + $proximity);
      $n = [];

      foreach ($lines as $neighborKey => $neighbors) {
        $neighbor = $textract[$neighbors];
        if (is_numeric($neighborKey)) {
          $neighborWidth = $neighbor['Geometry']['BoundingBox']['Width'];
          $neighborHeight = $neighbor['Geometry']['BoundingBox']['Height'];
          $neighborLeft = $neighbor['Geometry']['BoundingBox']['Left'];
          $neighborTop = $neighbor['Geometry']['BoundingBox']['Top'];
          if (($top <= $neighborTop && $height >= $neighborTop) ||
            ($top <= $neighborTop + $neighborHeight && $height >= $neighborTop + $neighborHeight)) {
            if (($left <= $neighborLeft && $width >= $neighborLeft) ||
              ($left <= $neighborLeft + $neighborWidth && $width >= $neighborLeft + $neighborWidth)) {

              $n[] = $neighbor; //if ($textKeys[$textKey]['keyValue'] != $neighbor['keyValue']) {
              //$textKeys[$textKey]['score'] += 1;
              //}
            }
          }
        }
      }
      $aa = array_search($primaryKey['Id'], array_column($n, 'Id'));
      if (is_numeric($aa)) {
        unset($n[$aa]);
      }
      $allNeighbor = [];
      foreach ($n as $nn) {
          array_push($allNeighbor, explode(' ', $nn['Text']));
      }
      //dd($allNeighbor);
      $allNeighbor = $this->multiArrayToSingle($allNeighbor);
      $allNeighbor = array_filter($allNeighbor, 'is_string');
      foreach ($allValues as $allValue) {
        if (($key = array_search($allValue, $allNeighbor)) !== false) {
          unset($allNeighbor[$key]);
        }
      }
      if (count($allNeighbor) < 10) {
        $proximity += 0.1;
        goto Proximity;
      }

dd($allNeighbor);
      $data[] = [
        "code" => $attribute['field_id'],
        "label" => $attribute['field_id'],
        "type" => 'string',
        "keyword" => [
          'primary' => $attribute['label']['fulltext'],
          'secondary' => $secondaryKeywords,
          "neighbor" => $allNeighbor
        ],
        'proximity' => $proximity,
        'bounding_box' => $textract[$line]['Geometry']['BoundingBox'],
        'attribute_value' => [
          'bounding_box' => $attributeValue['Geometry']['BoundingBox']
        ]
      ];
    }
//dd($data);
   // $templates = DB::table('orc_templates')->insert($data);
  }


  public function arrayStringExplode($arrays, $column)
  {
    $result = [];
    foreach ($arrays as $item) {
      array_push($result, explode(' ', $item[$column]));
    }
    return $result;
  }

  public function multiArrayToSingle($array)
  {
    $outputs = [];
    array_walk_recursive($array, function ($val) use (&$outputs) {
      $outputs[] = $val;
    });
    return $outputs;
  }

  public function test1()
  {
    $templates = DB::table('ocr_templates')->get()->toArray();
    //add foreach for multiple document
//dd($templates);
    //$filePath = 'ecm/staging/1620047473.jpg';
    //$filePath = 'ecm/staging/sample_invoice.png';
    $filePath = 'ecm/staging/1629881696.png';
    $files = [
      /* [
          'path' => 'ecm/staging/Hloom_1.png'
        ],*/
      [
        'path' => 'ecm/staging/1620047473.jpg'
      ], [
        'path' => 'ecm/staging/Hloom_2.png'
      ], [
        'path' => 'ecm/staging/Hloom_3.png'
      ],
    ];
    /*  $files = [
       [
          'path' => 'ecm/staging/sample_invoice.png'
        ]
      ];*/

    //dd($files);
    foreach ($files as $key => $file) {
      // dd($file);
      //$textract = AWSHelper::awsTextract($filePath);
      $textract = AWSHelper::awsTextract($file['path']);
      //dd($textract);
      $modelId = AWSHelper::validateTemplate($textract, $templates);
      $data[] = AWSHelper::extractData($textract, $templates[$modelId]);
    }


    dd($data);

  }

}
